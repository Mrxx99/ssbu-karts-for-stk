## Credits
From [OpenGameArt.org](https://opengameart.org/): 

License - CC0:

* Base kart: <https://opengameart.org/content/racing-kart> - my changes to it are not subject to non-commercial restrictions; original version by Keith at Fertile Soil Productions

License - GPLv2+ or CC-BY-SA 3.0:

* <https://opengameart.org/content/metal-plate> -> trak2_metal2b.tga - 
From the TRaK2 Texture set By Georges "TRaK" Grondin

* <https://opengameart.org/content/old-blue-white-metal> -> trak2_metal1a.tga - From the TRaK2 Texture set By Georges "TRaK" Grondin

* <https://opengameart.org/content/old-blue-grey-metal> (currently unused) -> trak2_metal1b.tga - From the TRaK2 Texture set By Georges "TRaK" Grondin

* <https://opengameart.org/content/old-blue-black-metal> (currently unused) -> trak2_metal1c.tga - From the TRaK2 Texture set By Georges "TRaK" Grondin

Background colors for the karts originate from <https://github.com/OpenRCT2/OpenRCT2/wiki/Widget-colours>, on the 3rd column of the table present over there.

From SuperTuxKart itself:

* Light cones - copied from Amanda's kart

* './source/empty/stkkart_leatherShoes_a.png' - The leather texture copied from Sara the Wizard's kart

* Wheels - copied from Suzanne's kart, and texture replaced with in-game version

The icons for the Smash fighters can be found at and at <https://www.spriters-resource.com/nintendo_switch/supersmashbrosultimate/>, ripped by Random Talking Bush. Alternate location: <https://drive.google.com/open?id=1KLU5EeIGGwCL1bJ4m_ERNe7DzS8MCSLS> 

This repository tracks the model files' thread about them at <https://www.vg-resource.com/thread-34836.html>
