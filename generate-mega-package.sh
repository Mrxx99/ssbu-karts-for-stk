#!/bin/sh
# Based from https://askubuntu.com/questions/590837/how-to-batch-compress-folders
karts_dir=karts/
archive_name=ssbu-karts-for-stk_karts-only
format=7z

7z a -mx7 -mmt "${archive_name%/}.${format}" "$karts_dir"
