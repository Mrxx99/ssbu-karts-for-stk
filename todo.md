# Things that can be improved for the karts, and what can be done

* Only the default costumes of every fighter are here for now. Alternate costumes will be worked on later, with those that give different characters and those providing gender variants being given higher priority.

* All fighters, regardless of their original size, are shrinked by .025 Blender units in order to fit in the kart. Unfortunately, the standard karts shipping with STK are larger than most of the ported characters, to the point where the eyes for the realistic ones are hard to see. This can be changed if I decide to switch to unique karts for most characters.

* Unique karts for most characters that better highlight their personality or home universes.

* Reconfigure some textures to look better, especially for realistic characters.

* More animations, such as winning and losing ones, as I'm quite inexperienced with 3D animation, and still learning things. I need feedback on animations that I should create for each fighter, as they are the only way I can make each of them stand out in some way. I have slots for these animations available, but only the first item I would really like to have for all fighters:
  * Winning and Losing - start-winning, start-winning-loop, end-winning, start-losing, start-losing-loop, end-losing
  * Jump - start-jump, start-end
  * Kart Selection Screen - selection-start, selection-end
  * Backpedal (Looking backwards on reversing the kart) - backpedal-left, backpedal, backpedal-right

* The hairbands, mantles, capes, scarves, bandanas, skirts, aprons, and long hair for these characters can be implemented as speed-weighted objects, so that they look like they're being blown by the wind during kart acceleration:
  * Bayonetta
  * Captain Falcon
  * Chrom
  * Corrin
  * Dark Pit
  * Daisy
  * Donkey Kong
  * Dr. Mario
  * Ganondorf
  * Greninja
  * Ike
  * Ken
  * King Dedede
  * King K. Rool
  * Lucina
  * Marth
  * Meta Knight
  * Palutena
  * Peach
  * Pit
  * Richter
  * Robin
  * Roy
  * Ryu
  * Sheik
  * Simon
  * Snake
  * Zelda
