## Prerequisites
**SuperTuxKart 0.9.3 or later is required to use this addon collection. All karts have been fixed to work with 0.10 and later.** The addons are shipped in SPM format (this format is unique to this game, not having appeared anywhere else yet), which only said versions of STK can read. If STK isn't already installed, download it from <https://supertuxkart.net/Download>, and follow the specific instructions for your operating system. If using a Linux/BSD distro, there should be a package for the most recent STK release; install it from there.

## How to use/install
Copy or symlink the contents of the 'karts/' directory to one of these paths, depending on your operating system:

* On Windows: %APPDATA%/supertuxkart/addons/karts

* On Linux distros/BSDs: ~/.local/share/supertuxkart/addons/karts

* On macOS: ~/Library/Application Support/supertuxkart/addons/karts

* On Android: <location of internal storage>/stk/home/.local/share/supertuxkart/addons/karts

Or copy individual karts to the addons directory, if not all of them are desired. Upon starting STK, you should see the loading screen fill up similar to below, assuming that no other addon karts are installed:
![STK Android 0.9.3 Loading Screen](screenshots/Screenshot_20190210-163449.png)

## Tips

* Your opponents cannot be chosen (unless you're playing a local multiplayer game, or starting the game from the command-line)! They are decided at random, and depending on how many AI karts are set up (up to 20 by default), more than 8 fighters may be seen in one round.

* The karts have their own group, named 'Smash-Fighters', so they don't conflict and overflow addon karts downloaded from the STK addons site, and it is easier to find them. Alt costumes of the fighters are placed in another group, as 'Alt-Costume-Fighters', as 'Smash-Fighters' is intended for the default costumes of the characters.

* To race only with the SSBU fighters, select the 'Smash-Fighters' tab before selecting the kart. To race with all other groups of karts, select the 'All' tab before selecting the kart. Choosing this latter option basically enables racing with a much larger crossover, though it is limited 

* Overwhelmed by the sheer number of karts newly installed? Select the random kart option to race as one of 73 Smash fighters without spending time on decisions.

* If you get tired of racing on the same track layouts, you can race in reverse if the track supports it. All the tracks shipping with STK support this mode, so there's technically double the tracks for the price of one!

* Every character shares the same kart, and a logo for the fictional in-game company Mandarava Motors appears at the back. It's there to imply that that company is mass-manufacturing that kart.

* Mr. Game & Watch is colored not black, but purple in order to make him more visible. Additionally, he appears in 3D, but not flattened. No worries, it's the same character.

* Olimar's helmet is tinted slightly blue, but his head should still be visible. This texture change is to make the helmet more visible.

## Other modes to try out in-game:

* Time trial - You can race not only by yourself, but also add in AI karts or other players for a better challenge.

* Follow the leader - Stay ahead of other karts, but don't overtake the leader. This mode does not use laps.

* Three strikes battle - Battle against other karts with powerups! This modes bears the most resemblence to the Smash games, but you drive around in a 3D track/arena, instead of fighting on 2D platforms.

* Soccer - Use your kart to push the soccer ball or other object into the opposite-colored goal, depending on if you picked red or blue. This kind of game has never been explored in any Smash game, so this is new for many of the characters.

* Easter egg hunt - Explore tracks to find blue easter eggs.

* Ghost replays - Race against ghosts to try beating them, or just watch what they did.
