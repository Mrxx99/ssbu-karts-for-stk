# Super Smash Bros. Ultimate Fighter karts for SuperTuxKart (STK)
A collection of addon karts for use with free/open source (FOSS) racing game [SuperTuxKart](https://supertuxkart.net/) based on the roster of Super Smash Bros. Ultimate (SSBU). 73 characters from that game will be included in this set, excluding the Mii Fighters as their customizability cannot be implemented in STK. Downloadable content (DLC) fighters will be included when their files are ripped, and they are made publicly available. **A total of 74 karts are provided by this collection so far.**
Each karts' directory is found under the original name that appears in the game data for SSBU, including any Japanese names used there. They are loaded in that order, but they use their international full names on the kart selection screen.
Both the kart files and the source Blender files are found here. This repository is self-contained, where everything that is needed is inside. The stk-media repo or stk-assets repositories are useful for working with the source files, but both are not required, which makes it possible to reuse the source files elsewhere.

Karts-only downloads: <https://drive.google.com/drive/folders/1Lu1aS4lUVr_MFaRcuUp3byLaolmFL8Hl?usp=sharing>

## How to use/install
Can be found in </install.md>

## Detailed Kart Information
Can be found in </kart-information.html>
There are some screenshots in the './screenshots/' directory that showcase what 13 karts look like in-game for a preview, for both the front and back of them. Each character is there based on the electronic resistor color code. This is one set of two screenshots, featuring Kirby on the Oliver's Math Class track and Mario on the Candela City track:
![Oliver's Math Class front](screenshots/olivermath-2019.02.11_04.42.06.png)
![Oliver's Math Class back](screenshots/candela_city-2019.02.11_05.18.47.png)

## Premise: How the SSBU Fighters got into the Mascot Kingdom
Can be found in </premise.md>

## What this project does/does not do

* Only fighter characters are within this collection's scope; bosses and assist trophies are unlikely to appear here unless suitable icons can be provided for them.

* Super Mario characters are part of this collection, but only those that appear in SSBU s fighters, so it also does part Mario Kart. There are more of the non-Mario characters though, so it's best to call it like Smash Bros. Kart.

* Tracks are not provided by this collection, so the characters will end up racing in an original world created for STK, the [Mascot Kingdom](https://supertuxkart.net/Style), and whatever else appears in its addon site. They won't race in more iconic locales unless tracks using them are ported to or created fot STK. This situation is somewhat similar to Super Smash Bros. Brawl's Subspace Emissary, where fighters appear in a mostly generic world with generic enemies.

* This is not a STK -> Smash Bros. complete transformation package; you'll still be playing STK as this addon collection only provides karts. Unless STK is forked or rebranded, it is not possible to completely remove the presence of Tux, Thunderbird (the namesake mascot of the Mozilla e-mail client acting as the game's referee), GNU, Nolok, and some other karts shipping with STK.

## Rationale
The purpose of this project is to satisfy three objectives at once (a.k.a. kill three birds with one stone):

* Satisfy some peoples' desires to see a crossover racing game featuring the SSB cast. The addon collection is also intended to provide an alternative to the fighting genre for those who are tired of fighting games, but still want a crossover of some sort.

* Practice 3D texturing and animation skills that can be reused in my future projects.

* Promote FOSS by showing people what STK is capable of doing, and that it isn't a mere ripoff of other kart racing games. *Disclosure: My real name appears in the credits for STK, where I am listed as a donor.*

## Current status of the package as a whole
**The addons are considered to be alpha status, and are being initially released as [minimum viable products](https://en.wikipedia.org/wiki/Minimum_viable_product).** That is, they work and have minimal animation (only steering animations that involve the characters turning their heads), but are not really polished like the karts that ship with STK. Don't expect things to be grandeur at the moment; I'm not fully experienced with 3D animation, but I'm getting there.
All of the fighters share a single kart linked to their source files to make it easier to manage them, but it caused a loss in showing off some uniqueness of each character. See </todo.md> for more information.

### Related Resources
This project follows <https://gitlab.com/Worldblender/smash-ultimate-models-exported> for the status of characters. Head over there to use the characters for things other than kart racing.
[Official STK addon website](https://online.supertuxkart.net/) (I strongly urge against uploading this kart collection to over there; they will likely be rejected due to incompatible licensing of the characters appearing in the karts)

### Credits
Can be found in </credits.md>

## Licensing
**Unless otherwise specified below, everything in './karts/', './source/default_costumes/', and './source/alt_costumes/' is licensed under the CC-BY-NC 4.0 license.** Obviously, however, I do not claim any rights over the characters appearing in the karts; this is purely a fan creation. Now comes the specific items that are exempt from noncommercial restrictions:

* Original documentation - CC-BY-SA 4.0

* Original scripts - CC0

* In './source/empty/*' (this also applies to copies of these files appearing in './karts/'):
  * kart-base*.*, genericshadow.png - CC-BY-SA 4.0
  * applications-other* - Tango Icon Project
  * stk*.png - CC-BY-SA 3.0 - SuperTuxKart textures
  * trak2_*.png - GPLv2+ or CC-BY-SA 3.0 - TRaK2 Texture set By Georges "TRaK" Grondin

* In './source/default_costumes/gamewatch/':
  * stktex_stonePurple_*.png - CC-BY-SA 3.0 - SuperTuxKart textures

* In './source/default_costumes/pikmin/':
  * stk_glassPanel_a.png - CC-BY-SA 3.0 - SuperTuxKart textures
